<?php
  use \Psr\Http\Message\ServerRequestInterface as Request;
  use \Psr\Http\Message\ResponseInterface as Response;

  // Get All Event
  $app->get('/api/events', function(Request $request, Response $response)
  {
    $sql = "SELECT * FROM event;";
    try {
      //Get DB Object
      $db = new db();
      //Connect
      $db = $db->connect();

      $statement = $db->query($sql);
      $events = $statement->fetchAll(PDO::FETCH_OBJ);
      $db = null;
      return $response->withJson($events);
    } catch (PDOException $e) {
      $err = array('error' => $e->getMessage(), );
      return $response->withJson($err);
    }
  });

  // Get Single Author
  $app->get('/api/event/{id}', function(Request $request, Response $response)
  {
    $id = $request->getAttribute('id');
    $sql = "SELECT * FROM event WHERE id = $id";
    try {
      //Get DB Object
      $db = new db();
      //Connect
      $db = $db->connect();

      $statement = $db->query($sql);
      $event = $statement->fetchAll(PDO::FETCH_OBJ);
      $db = null;
      return $response->withJson($event);
    } catch (PDOException $e) {
      $err = array('error' => $e->getMessage(), );
      return $response->withJson($err);
    }
  });

  // Add New Author
  $app->post('/api/event/add', function(Request $request, Response $response)
  {

    $id = $request->getParam('id');
    $id_organisasi = $request->getParam('id_organisasi');
    $id_author = $request->getParam('id_author');
    $judul = $request->getParam('judul');
    $tanggal_mulai = $request->getParam('waktu');
    $tanggal_selesai = $request->getParam('deskripsi');
    $tempat = $request->getParam('tempat');

    $sql = "INSERT INTO
            event(
              id,
              id_organisasi,
              id_author,
              judul,
              waktu,
              deskripsi,
              tempat
            )
            VALUES (
              :id,
              :id_organisasi,
              :id_author,
              :judul,
              :waktu,
              :deskripsi,
              :tempat
            )";
    try {
      //Get DB Object
      $db = new db();
      //Connect
      $db = $db->connect();

      $statement = $db->prepare($sql);

      $statement->bindParam(':id', $id);
      $statement->bindParam(':id_organisasi', $id_organisasi);
      $statement->bindParam(':id_author', $id_author);
      $statement->bindParam(':judul', $judul);
      $statement->bindParam(':waktu', $waktu);
      $statement->bindParam(':deskripsi', $deskripsi);
      $statement->bindParam(':tempat', $tempat);

      $statement->execute();
      $db=null;
      $success = array('status' => 'success');
      return $response->withJson($success);
    } catch (PDOException $e) {
      $err = array('status' => $e->getMessage(), );
      echo $err;
    }
  });

  //Update Author
  $app->put('/api/event/update/{id}', function (Request $request, Response $response)
  {

    $id = $request->getAttribute('id');

    $id = $request->getParam('id');
    $id_organisasi = $request->getParam('id_organisasi');
    $id_author = $request->getParam('id_author');
    $judul = $request->getParam('judul');
    $tanggal_mulai = $request->getParam('waktu');
    $tanggal_selesai = $request->getParam('deskripsi');
    $tempat = $request->getParam('tempat');

    $sql = "UPDATE event SET
         id    = :id,
         id_organisasi   = :id_organisasi,
         id_author = :id_author,
         judul = :judul,
         waktu  = :waktu,
         deskripsi  = :deskripsi,
         tempat  = :tempat
         WHERE id = $id";

    try {
      $db = new db();
      $db = $db->connect();
      $statement = $db->prepare($sql);

      $statement->bindParam(':id', $id);
      $statement->bindParam(':id_organisasi', $id_organisasi);
      $statement->bindParam(':id_author', $id_author);
      $statement->bindParam(':judul', $judul);
      $statement->bindParam(':waktu', $waktu);
      $statement->bindParam(':deskripsi', $deskripsi);
      $statement->bindParam(':tempat', $tempat);

      $statement->execute();
      $db=null;
      $success = array('status' => 'success');
      return $response->withJson($success);
    } catch (PDOException $e) {
      $err = array('error' => $e->getMessage(), );
      return $response->withHeader('Content-type', 'application/json')
            ->withJson($err);
    }
  });


  //Delete Author
  $app->delete('/api/event/delete/{id}', function(Request $request, Response $response){
      $id = $request->getAttribute('id');

      $sql = "DELETE FROM event WHERE id = $id";

      try{
          // Get DB Object
          $db = new db();
          // Connect
          $db = $db->connect();

          $stmt = $db->prepare($sql);
          $stmt->execute();
          $db = null;
          $success = array('status' => 'success');
          return $response->withJson($success);
      } catch(PDOException $e){
        $err = array('error' => $e->getMessage(), );
        return $response->withJson($err);
    }
  });
