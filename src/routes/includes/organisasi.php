<?php
  use \Psr\Http\Message\ServerRequestInterface as Request;
  use \Psr\Http\Message\ResponseInterface as Response;

  // Get All Organisasi
  $app->get('/api/organisasi', function(Request $request, Response $response)
  {
    $sql = "SELECT * FROM organisasi;";
    try {
      //Get DB Object
      $db = new db();
      //Connect
      $db = $db->connect();

      $statement = $db->query($sql);
      $organisasi = $statement->fetchAll(PDO::FETCH_OBJ);
      $db = null;
      return $response->withJson($organisasi);
    } catch (PDOException $e) {
      $err = array('error' => $e->getMessage(), );
      return $response->withJson($err);
    }
  });

  // Get Single organisasi
  $app->get('/api/organisasi/{id}', function(Request $request, Response $response)
  {
    $id = $request->getAttribute('id');
    $sql = "SELECT * FROM organisasi WHERE id = $id";
    try {
      //Get DB Object
      $db = new db();
      //Connect
      $db = $db->connect();

      $statement = $db->query($sql);
      $organisasi = $statement->fetchAll(PDO::FETCH_OBJ);
      $db = null;
      return $response->withJson($organisasi);
    } catch (PDOException $e) {
      $err = array('error' => $e->getMessage(), );
      return $response->withJson($err);
    }
  });

  // Add New organisasi
  $app->post('/api/organisasi/add', function(Request $request, Response $response)
  {

    $id = $request->getParam('id');
    $nama = $request->getParam('nama');
    $deskripsi = $request->getParam('deskripsi');
    $internal = $request->getParam('internal');

    $sql = "INSERT INTO
            organisasi(
              id,
              nama,
              deskripsi,
              internal
            )
            VALUES (
              :id,
              :nama,
              :deskripsi,
              :internal
            )";
    try {
      //Get DB Object
      $db = new db();
      //Connect
      $db = $db->connect();

      $statement = $db->prepare($sql);

      $statement->bindParam(':id', $id);
      $statement->bindParam(':nama', $nama);
      $statement->bindParam(':deskripsi', $deskripsi);
      $statement->bindParam(':internal', $internal);

      $statement->execute();
      $db=null;
      $success = array('status' => 'success');
      return $response->withJson($success);
    } catch (PDOException $e) {
      $err = array('status' => $e->getMessage(), );
      echo $err;
    }
  });

  //Update organisasi
  $app->put('/api/organisasi/update/{id}', function (Request $request, Response $response)
  {

    $id = $request->getAttribute('id');

    $id = $request->getParam('id');
    $nama = $request->getParam('nama');
    $deskripsi = $request->getParam('deskripsi');
    $internal = $request->getParam('internal');


    $sql = "UPDATE organisasi SET
         id    = :id,
         nama   = :nama,
         deskripsi   = :deskripsi,
         internal = :internal
         WHERE id = $id";

    try {
      $db = new db();
      $db = $db->connect();
      $statement = $db->prepare($sql);

      $statement->bindParam(':id', $id);
      $statement->bindParam(':nama', $nama);
      $statement->bindParam(':deskripsi', $deskripsi);
      $statement->bindParam(':internal', $internal);

      $statement->execute();
      $db=null;
      $success = array('status' => 'success');
      return $response->withJson($success);
    } catch (PDOException $e) {
      $err = array('error' => $e->getMessage(), );
      return $response->withHeader('Content-type', 'application/json')
            ->withJson($err);
    }
  });


  //Delete organisasi
  $app->delete('/api/organisasi/delete/{id}', function(Request $request, Response $response){
      $id = $request->getAttribute('id');

      $sql = "DELETE FROM organisasi WHERE id = $id";

      try{
          // Get DB Object
          $db = new db();
          // Connect
          $db = $db->connect();

          $stmt = $db->prepare($sql);
          $stmt->execute();
          $db = null;
          $success = array('status' => 'success');
          return $response->withJson($success);
      } catch(PDOException $e){
        $err = array('error' => $e->getMessage(), );
        return $response->withJson($err);
    }
  });
