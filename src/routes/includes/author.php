<?php
  use \Psr\Http\Message\ServerRequestInterface as Request;
  use \Psr\Http\Message\ResponseInterface as Response;

  // Get All Authors
  $app->get('/api/authors', function(Request $request, Response $response)
  {
    $sql = "SELECT * FROM author;";
    try {
      //Get DB Object
      $db = new db();
      //Connect
      $db = $db->connect();

      $statement = $db->query($sql);
      $authors = $statement->fetchAll(PDO::FETCH_OBJ);
      $db = null;
      return $response->withJson($authors);
    } catch (PDOException $e) {
      $err = array('error' => $e->getMessage(), );
      return $response->withJson($err);
    }
  });

  // Get Single Author
  $app->get('/api/author/{id}', function(Request $request, Response $response)
  {
    $id = $request->getAttribute('id');
    $sql = "SELECT * FROM author WHERE id = $id";
    try {
      //Get DB Object
      $db = new db();
      //Connect
      $db = $db->connect();

      $statement = $db->query($sql);
      $author = $statement->fetchAll(PDO::FETCH_OBJ);
      $db = null;
      return $response->withJson($author);
    } catch (PDOException $e) {
      $err = array('error' => $e->getMessage(), );
      return $response->withJson($err);
    }
  });

  // Add New Author
  $app->post('/api/author/add', function(Request $request, Response $response)
  {

    $id = $request->getParam('id');
    $nama = $request->getParam('nama');
    $nim = $request->getParam('nim');
    $telpon = $request->getParam('telpon');
    $email = $request->getParam('email');
    $alamat = $request->getParam('alamat');

    $sql = "INSERT INTO author(id, nama, nim, telpon, email, alamat)
            VALUES (:id, :nama,:nim, :telpon, :email, :alamat)";
    try {
      //Get DB Object
      $db = new db();
      //Connect
      $db = $db->connect();

      $statement = $db->prepare($sql);

      $statement->bindParam(':id', $id);
      $statement->bindParam(':nama', $nama);
      $statement->bindParam(':nim', $nim);
      $statement->bindParam(':telpon', $telpon);
      $statement->bindParam(':email', $email);
      $statement->bindParam(':alamat', $alamat);

      $statement->execute();
      $db=null;
      $success = array('status' => 'success');
      return $response->withJson($success);
    } catch (PDOException $e) {
      $err = array('status' => $e->getMessage(), );
      echo $err;
    }
  });

  //Update Author
  $app->put('/api/author/update/{id}', function (Request $request, Response $response)
  {

    $id = $request->getAttribute('id');

    $id = $request->getParam('id');
    $nama = $request->getParam('nama');
    $nim = $request->getParam('nim');
    $telpon = $request->getParam('telpon');
    $email = $request->getParam('email');
    $alamat = $request->getParam('alamat');

    $sql = "UPDATE author SET
         id    = :id,
         nama   = :nama,
         nim   = :nim,
         telpon = :telpon,
         email  = :email,
         alamat = :alamat
         WHERE id = $id";

    try {
      $db = new db();
      $db = $db->connect();
      $statement = $db->prepare($sql);

      $statement->bindParam(':id', $id);
      $statement->bindParam(':nama', $nama);
      $statement->bindParam(':nim', $nim);
      $statement->bindParam(':telpon', $telpon);
      $statement->bindParam(':email', $email);
      $statement->bindParam(':alamat', $alamat);

      $statement->execute();
      $db=null;
      $success = array('status' => 'success');
      return $response->withJson($success);
    } catch (PDOException $e) {
      $err = array('error' => $e->getMessage(), );
      return $response->withHeader('Content-type', 'application/json')
            ->withJson($err);
    }
  });


  //Delete Author
  $app->delete('/api/author/delete/{id}', function(Request $request, Response $response){
      $id = $request->getAttribute('id');

      $sql = "DELETE FROM author WHERE id = $id";

      try{
          // Get DB Object
          $db = new db();
          // Connect
          $db = $db->connect();

          $stmt = $db->prepare($sql);
          $stmt->execute();
          $db = null;
          $success = array('status' => 'success');
          return $response->withJson($success);
      } catch(PDOException $e){
        $err = array('error' => $e->getMessage(), );
        return $response->withJson($err);
    }
  });
