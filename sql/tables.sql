/*
 * The MIT License
 *
 * Copyright 2017 su.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
/**
 * Author:  su
 * Created: Oct 10, 2017
 */

use event_amikom;
create table organisasi(
    id int(10) primary key auto_increment,
    nama varchar(50) not null,
    deskripsi text,
    internal enum('true', 'false') default 'false'
)engine=innodb;

create table author(
    id int(12) primary key auto_increment,
    nama varchar(50) not null,
    nim varchar(10) not null,
    telpon varchar(15) not null,
    email varchar(50),
    alamat varchar(100)
)engine=innodb;

create table login_author(
    id_author int(12) not null,
    akses enum('admin','user') default 'user',
    pass varchar(25) not null
    foreign key (id_author) references author(id)
)engine=innodb;

create table event(
    id int(24) primary key auto_increment,
    id_organisasi int(10) not null,
    id_author int(12) not null,
    judul varchar(60) not null,
    waktu date not null,
    deskripsi text,
    tempat varchar(60) not null,
    foreign key (id_organisasi) references organisasi(id),
    foreign key (id_author) references author(id)
)engine=innodb;
